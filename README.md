# Kind with CNI

This is just kind with simple CNI installed from https://github.com/containernetworking/plugins/

It enables to speed up capd tests, as it prevents us from having to install a CNI (like calico) in each deployment.

In order to build a new image using a specific kind version as base, add a tag to this project corresponding to that image tag.

The list of kind tags can be retrieved here: https://hub.docker.com/r/kindest/node/tags
