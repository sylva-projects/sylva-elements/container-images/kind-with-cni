ARG KIND_VERSION

FROM kindest/node:${KIND_VERSION}

RUN curl -sSL --retry 5 --output /tmp/cni.tgz https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz \
 && mkdir -p /opt/cni/bin \
 && tar -C /opt/cni/bin -xzf /tmp/cni.tgz \
 && rm -rf /tmp/cni.tgz
